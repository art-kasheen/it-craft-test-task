import { products } from '../fixtures'

export const getAllProducts = async () => {
  return new Promise((resolve) => {
    resolve(products)
  })
}
