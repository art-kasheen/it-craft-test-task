export const listToMap = (list) => {
  return list.reduce((acc, value) => {
    return {
      ...acc,
      [value.id]: value
    }
  }, {})
}
