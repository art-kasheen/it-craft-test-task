import React from 'react'
import { shallow } from 'enzyme'
import { ProductsList } from './index'
import { products } from '../../fixtures'

describe('products-list', () => {
  const fetchProducts = jest.fn()
  const defaultProps = {
    fetchProducts: fetchProducts,
    products: products
  }

  it('should render loading', () => {
    const props = {
      ...defaultProps,
      loading: true
    }

    const wrapper = shallow(<ProductsList {...props} />)

    expect(wrapper.find('[data-test="product-loading"]').length).toEqual(1)
  })

  it('should render products', () => {
    const wrapper = shallow(<ProductsList {...defaultProps} />)

    expect(wrapper.find('[data-test="products-item"]').length).toEqual(
      products.length
    )
  })

  it('should fetch products', () => {
    shallow(<ProductsList {...defaultProps} />)

    expect(fetchProducts).toHaveBeenCalled()
  })
})
