import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { fetchProducts } from '../../actions'
import { getProducts } from '../../selectors'
import ProductCard from '../product-card'
import './styles.sass'

export class ProductsList extends Component {
  static propTypes = {
    products: PropTypes.array,
    fetchProducts: PropTypes.func.isRequired
  }

  componentDidMount() {
    this.props.fetchProducts()
  }

  render() {
    const { products, loading } = this.props

    if (loading) return <p data-test="product-loading">Loading ...</p>

    return (
      <div className="products">
        <ul className="products__list">
          {products &&
            products.map((product) => (
              <li
                data-test="products-item"
                className="products__item"
                key={product.id}
              >
                <ProductCard product={product} />
              </li>
            ))}
        </ul>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    products: getProducts(state),
    loading: state.products.loading
  }),
  { fetchProducts }
)(ProductsList)
