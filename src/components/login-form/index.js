import React from 'react'
import PropTypes from 'prop-types'
import { Formik } from 'formik'
import { connect } from 'react-redux'
import { login } from '../../actions'
import './styles.sass'

const LoginForm = ({ login }) => {
  return (
    <Formik
      initialValues={{ email: '', password: '' }}
      validate={(values) => {
        let errors = {}

        const isValidField = (type) => {
          if (!values[type]) {
            errors[type] = 'This field is required'
          } else if (values[type].length <= 8) {
            errors[type] = 'The value of the field is too short'
          } else {
            return
          }
        }

        Object.keys(values).forEach((value) => isValidField(value))

        return errors
      }}
      onSubmit={(values, { setSubmitting }) => {
        login(values)
        setSubmitting(false)
      }}
      render={({
        values,
        errors,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting
      }) => (
        <form onSubmit={handleSubmit}>
          <div className="login-form">
            <h1 className="title">Login</h1>

            <div className="login-form__field">
              <input
                type="text"
                name="email"
                placeholder="Email"
                className="login-form__input"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />

              {errors.email && touched.email && (
                <p className="login-form__message">{errors.email}</p>
              )}
            </div>

            <div className="login-form__field">
              <input
                type="password"
                name="password"
                className="login-form__input"
                placeholder="Password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
              {errors.password && touched.password && (
                <p className="login-form__message">{errors.password}</p>
              )}
            </div>

            <button type="submit" className="button" disabled={isSubmitting}>
              Submit
            </button>
          </div>
        </form>
      )}
    />
  )
}

LoginForm.propTypes = {
  login: PropTypes.func.isRequired
}

export default connect(
  null,
  { login }
)(LoginForm)
