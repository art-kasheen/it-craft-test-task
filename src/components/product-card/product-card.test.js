import React from 'react'
import { shallow } from 'enzyme'
import { ProductCard } from './index'
import { products } from '../../fixtures'

describe('product-card', () => {
  it('should call addToProduct', () => {
    const handleClick = jest.fn()

    const wrapper = shallow(
      <ProductCard
        addToCart={handleClick}
        isProductInCart={false}
        product={products[0]}
      />
    )

    wrapper.find('[data-test="add-to-cart"]').simulate('click')

    expect(handleClick).toHaveBeenCalled()
  })

  it('should render go to cart button when the product already added to cart', () => {
    const handleClick = jest.fn()

    const wrapper = shallow(
      <ProductCard
        addToCart={handleClick}
        isProductInCart={true}
        product={products[0]}
      />
    )

    expect(wrapper.find('[data-test="add-to-cart"]').length).toBe(0)
    expect(wrapper.find('[data-test="go-to-cart"]').length).toBe(1)
  })
})
