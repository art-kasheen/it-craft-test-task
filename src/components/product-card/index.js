import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { addToCart } from '../../actions'
import { isProductInCart } from '../../selectors'
import { history } from '../../history'
import './styles.sass'

export const ProductCard = ({ product, addToCart, isProductInCart }) => {
  const handleAddToCart = (id) => () => {
    addToCart(id)
  }

  const handleGoToCart = () => {
    history.push('/cart')
  }

  return (
    <div className="product">
      <div className="product__image">
        <img src={product.image} alt={product.model} />
      </div>
      <h3 className="product__title">
        {product.make} {product.model} <small>{product.year}</small>
      </h3>
      <p className="product__desc">{product.description}</p>
      <p className="product__price">${product.price}</p>

      {isProductInCart ? (
        <button
          className="button"
          data-test="go-to-cart"
          onClick={handleGoToCart}
        >
          Go to cart
        </button>
      ) : (
        <button
          className="button"
          data-test="add-to-cart"
          onClick={handleAddToCart(product.id)}
        >
          Add to cart
        </button>
      )}
    </div>
  )
}

ProductCard.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    make: PropTypes.string,
    model: PropTypes.string,
    year: PropTypes.number,
    description: PropTypes.string,
    price: PropTypes.string
  }),
  addToCart: PropTypes.func.isRequired,
  isProductInCart: PropTypes.bool.isRequired
}

export default connect(
  (state, props) => ({
    isProductInCart: isProductInCart(state, props.product.id)
  }),
  { addToCart }
)(ProductCard)
