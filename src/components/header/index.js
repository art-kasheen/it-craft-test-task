import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { logout } from '../../actions'
import './styles.sass'
import { getCartAmount, getUser } from '../../selectors'

export const Header = ({ userName, cartAmount, logout }) => {
  const handleLogOut = () => {
    logout()
  }

  return (
    <header className="header">
      <div className="header__container">
        <div className="header__left-side">
          <Link to="/" className="logo">
            Store
          </Link>
        </div>

        <div className="header__right-side">
          <Link to="/cart" data-test="cart-link" className="header__cart-link">
            Cart ({cartAmount})
          </Link>

          {userName && (
            <>
              <span className="header__username">Hi, {userName}</span>
              <button className="header__logout-btn" onClick={handleLogOut}>
                Logout
              </button>
            </>
          )}
        </div>
      </div>
    </header>
  )
}

Header.propTypes = {
  userName: PropTypes.string.isRequired,
  logout: PropTypes.func.isRequired,
  cartAmount: PropTypes.number.isRequired
}

export default connect(
  (state) => ({
    user: getUser(state),
    cartAmount: getCartAmount(state)
  }),
  { logout }
)(Header)
