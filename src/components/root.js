import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { Switch, Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { initUser } from '../actions'
import { getUser } from '../selectors'
import Login from './routes/login-page'
import Market from './routes/market-page'
import Cart from './routes/cart-page'
import Header from './header'

const Root = ({ user, initUser }) => {
  useEffect(() => {
    initUser()
    // eslint-disable-next-line
  }, [])

  return (
    <>
      <Header userName={user.userName} />
      <Switch>
        <Redirect exact from="/" to="/login" />
        {user.isLoggedIn && <Redirect exact from="/login" to="/market" />}
        <Route exact path="/login" component={Login} />
        <Route exact path="/market" component={Market} />
        <Route exact path="/cart" component={Cart} />
      </Switch>
    </>
  )
}

Root.propTypes = {
  user: PropTypes.shape({
    userName: PropTypes.string,
    isLogedIn: PropTypes.bool
  }).isRequired,
  initUser: PropTypes.func.isRequired
}

export default connect(
  (state) => ({
    user: getUser(state)
  }),
  { initUser }
)(Root)
