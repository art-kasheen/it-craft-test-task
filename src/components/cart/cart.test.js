import React from 'react'
import { shallow } from 'enzyme'
import { Cart } from './index'
import { products } from '../../fixtures'

describe('cart', () => {
  const handleDelete = jest.fn()
  const defaultProps = {
    products: products,
    deleteFromCart: handleDelete
  }

  it('should render products in cart', () => {
    const wrapper = shallow(<Cart {...defaultProps} />)

    expect(wrapper.find('[data-test="cart-item"]').length).toEqual(
      products.length
    )
  })

  it('should delete products from cart', () => {
    const wrapper = shallow(<Cart {...defaultProps} />)

    wrapper
      .find('[data-test="cart-item"]')
      .at(1)
      .find('[data-test="delete-from-cart"]')
      .simulate('click')

    expect(handleDelete).toHaveBeenCalled()
  })

  it('should render nothing found when cart is empty', () => {
    const props = {
      ...defaultProps,
      products: []
    }

    const wrapper = shallow(<Cart {...props} />)

    expect(wrapper.find('[data-test="cart-nothing-added"]').length).toBe(1)
  })
})
