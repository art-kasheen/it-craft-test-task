import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { deleteFromCart } from '../../actions'
import { getProductsFromCart } from '../../selectors'
import { Link } from 'react-router-dom'
import './styles.sass'

export const Cart = ({ products, deleteFromCart }) => {
  const handleDelete = (id) => () => {
    deleteFromCart(id)
  }

  const getCartItem = (product) => (
    <li key={product.id} data-test="cart-item" className="cart__item item-cart">
      <h3 className="item-cart__title">
        {product.make} {product.model} <small>{product.year}</small>
      </h3>
      <span className="item-cart__desc">{product.description}</span>
      <span className="item-cart__price">${product.price}</span>

      <button
        className="item-cart__button button"
        data-test="delete-from-cart"
        onClick={handleDelete(product.id)}
      >
        delete
      </button>
    </li>
  )

  if (!products.length)
    return (
      <p data-test="cart-nothing-added">
        Nothing added to cart. Add some at <Link to="/market">market</Link>.
      </p>
    )

  return (
    <div className="cart" data-test="cart">
      <ul className="cart__list">
        {products.map((product) => getCartItem(product))}
      </ul>
    </div>
  )
}

Cart.propTypes = {
  products: PropTypes.array,
  deleteFromCart: PropTypes.func.isRequired
}

export default connect(
  (state) => ({
    products: getProductsFromCart(state)
  }),
  { deleteFromCart }
)(Cart)
