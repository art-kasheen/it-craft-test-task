import React from 'react'
import Cart from '../../cart'
import './styles.sass'

const CartPage = () => {
  return (
    <div className="cart-page">
      <div className="cart-page__container">
        <h1 className="title">Cart</h1>

        <Cart />
      </div>
    </div>
  )
}

export default CartPage
