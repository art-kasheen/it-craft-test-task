import React from 'react'
import LoginForm from '../../login-form'
import './styles.sass'

export default () => {
  return (
    <div className="login-page">
      <div className="login-page__container">
        <LoginForm />
      </div>
    </div>
  )
}
