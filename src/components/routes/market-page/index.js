import React from 'react'
import ProductsList from '../../products-list'
import './styles.sass'

const MarketPage = () => {
  return (
    <div className="market-page">
      <div className="market-page__container">
        <h1 className="title">Products</h1>

        <ProductsList />
      </div>
    </div>
  )
}

export default MarketPage
