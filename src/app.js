import React from 'react'
import { Provider } from 'react-redux'
import { Router } from 'react-router-dom'
import store from './store'
import Root from './components/root'
import { history } from './history'
import './styles/global.sass'

function App() {
  return (
    <Router history={history}>
      <Provider store={store}>
        <Root />
      </Provider>
    </Router>
  )
}

export default App
