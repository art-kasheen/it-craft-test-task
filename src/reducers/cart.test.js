import { ADD_TO_CART, DELETE_FROM_CART } from '../constants'
import reducer from './cart'

describe('cart reducer', () => {
  it('ADD_TO_CART', () => {
    const action = {
      type: ADD_TO_CART,
      payload: 1
    }

    const prevState = {
      products: []
    }

    const expectedState = reducer(prevState, action)

    expect({
      products: [1]
    }).toEqual(expectedState)
  })

  it('DELETE_FROM_CART', () => {
    const action = {
      type: DELETE_FROM_CART,
      payload: 2
    }

    const prevState = {
      products: [1, 2, 3]
    }

    const expectedState = reducer(prevState, action)

    expect({
      products: [1, 3]
    }).toEqual(expectedState)
  })
})
