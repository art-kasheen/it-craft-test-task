import { LOGIN, LOGOUT } from '../constants'

const initialState = {
  userName: '',
  isLoggedIn: false
}

export default (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case LOGIN:
      return {
        ...state,
        userName: payload.login,
        isLoggedIn: true
      }

    case LOGOUT:
      return {
        ...state,
        userName: '',
        isLoggedIn: false
      }

    default:
      return state
  }
}
