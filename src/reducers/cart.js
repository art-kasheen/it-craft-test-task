import { ADD_TO_CART, DELETE_FROM_CART } from '../constants'

const initialState = {
  products: []
}

export default (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case ADD_TO_CART:
      return {
        ...state,
        products: state.products.concat(payload)
      }

    case DELETE_FROM_CART:
      return {
        ...state,
        products: state.products.filter((id) => id !== payload)
      }

    default:
      return state
  }
}
