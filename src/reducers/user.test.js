import { LOGIN, LOGOUT } from '../constants'
import reducer from './user'

describe('user reducer', () => {
  it('LOGIN', () => {
    const action = {
      type: LOGIN,
      payload: { login: 'tester' }
    }

    const prevState = {
      userName: '',
      isLoggedIn: false
    }

    const expectedState = reducer(prevState, action)

    expect({
      userName: 'tester',
      isLoggedIn: true
    }).toEqual(expectedState)
  })

  it('LOGOUT', () => {
    const action = {
      type: LOGOUT
    }

    const prevState = {
      userName: 'tester',
      isLoggedIn: true
    }

    const expectedState = reducer(prevState, action)

    expect({
      userName: '',
      isLoggedIn: false
    }).toEqual(expectedState)
  })
})
