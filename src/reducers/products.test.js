import { FETCH_PRODUCTS_REQUEST, FETCH_PRODUCTS_SUCCESS } from '../constants'
import reducer from './products'
import { products } from '../fixtures'
import { listToMap } from '../services/helpers'

describe('products reducer', () => {
  it('FETCH_PRODUCTS_REQUEST', () => {
    const action = {
      type: FETCH_PRODUCTS_REQUEST
    }

    const prevState = {
      loading: false,
      data: null
    }

    const expectedState = reducer(prevState, action)

    expect({
      loading: true,
      data: null
    }).toEqual(expectedState)
  })

  it('FETCH_PRODUCTS_REQUEST', () => {
    const action = {
      type: FETCH_PRODUCTS_SUCCESS,
      payload: products
    }

    const prevState = {
      loading: true,
      data: null
    }

    const expectedState = reducer(prevState, action)

    expect({
      loading: false,
      data: listToMap(products)
    }).toEqual(expectedState)
  })
})
