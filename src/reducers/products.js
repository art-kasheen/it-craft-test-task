import { FETCH_PRODUCTS_REQUEST, FETCH_PRODUCTS_SUCCESS } from '../constants'
import { listToMap } from '../services/helpers'

const initialState = {
  loading: false,
  data: null
}

export default (state = initialState, action) => {
  const { type, payload } = action

  switch (type) {
    case FETCH_PRODUCTS_REQUEST:
      return {
        ...state,
        loading: true
      }

    case FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        data: listToMap(payload),
        loading: false
      }

    default:
      return state
  }
}
