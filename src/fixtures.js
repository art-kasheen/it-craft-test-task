export const products = [
  {
    id: 1,
    model: 'ZDX',
    make: 'Acura',
    year: 2010,
    price: '5851792795',
    description: 'Hepatobiliary System and Pancreas, Excision',
    image: 'http://dummyimage.com/400x250.jpg/ff4444/ffffff'
  },
  {
    id: 2,
    model: 'Lancer',
    make: 'Mitsubishi',
    year: 2009,
    price: '4998790765',
    description: 'Excision of Left Epididymis, Perc Endo Approach, Diagn',
    image: 'http://dummyimage.com/400x250.jpg/cc0000/ffffff'
  },
  {
    id: 3,
    model: 'SC',
    make: 'Lexus',
    year: 1996,
    price: '4215739608',
    description: 'Destruction of Right Eustachian Tube, Percutaneous Approach',
    image: 'http://dummyimage.com/400x250.jpg/5fa2dd/ffffff'
  },
  {
    id: 4,
    model: 'Leone',
    make: 'Subaru',
    year: 1988,
    price: '3928898841',
    description: 'Resection of Left Parotid Gland, Open Approach',
    image: 'http://dummyimage.com/400x250.jpg/ff4444/ffffff'
  },
  {
    id: 5,
    model: 'Bronco',
    make: 'Ford',
    year: 1991,
    price: '9225307594',
    description: 'Inspection of Right Elbow Region, Percutaneous Approach',
    image: 'http://dummyimage.com/400x250.jpg/dddddd/000000'
  },
  {
    id: 6,
    model: 'Celica',
    make: 'Toyota',
    year: 1976,
    price: '0695465775',
    description: 'Dilate L Com Iliac Art, Bifurc, w 2 Drug-elut, Open',
    image: 'http://dummyimage.com/400x250.jpg/5fa2dd/ffffff'
  },
  {
    id: 7,
    model: 'SL-Class',
    make: 'Mercedes-Benz',
    year: 1997,
    price: '4743949459',
    description: 'Examination of Nervous System',
    image: 'http://dummyimage.com/400x250.jpg/dddddd/000000'
  },
  {
    id: 8,
    model: 'Suburban 1500',
    make: 'GMC',
    year: 1993,
    price: '2351943910',
    description: 'Transplantation of Nervous System into POC, Perc Approach',
    image: 'http://dummyimage.com/400x250.jpg/5fa2dd/ffffff'
  },
  {
    id: 9,
    model: 'Silverado 3500',
    make: 'Chevrolet',
    year: 2009,
    price: '4362542639',
    description: 'Fluoroscopy of Epidural Veins using H Osm Contrast, Guidance',
    image: 'http://dummyimage.com/400x250.jpg/cc0000/ffffff'
  },
  {
    id: 10,
    model: 'M',
    make: 'Infiniti',
    year: 2011,
    price: '4453156839',
    description: 'Imaging, Veins, Magnetic Resonance Imaging (MRI)',
    image: 'http://dummyimage.com/400x250.jpg/cc0000/ffffff'
  }
]
