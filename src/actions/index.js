import {
  LOGIN,
  LOGOUT,
  FETCH_PRODUCTS_REQUEST,
  FETCH_PRODUCTS_SUCCESS,
  ADD_TO_CART,
  DELETE_FROM_CART
} from '../constants'
import { history } from '../history'
import { getAllProducts } from '../services/api'

export const initUser = () => (dispatch) => {
  const user = localStorage.getItem('my-store:user')

  if (user) {
    dispatch({
      type: LOGIN,
      payload: { login: user }
    })
  }
}

export const login = (user) => (dispatch) => {
  localStorage.setItem('my-store:user', user.email)

  dispatch({
    type: LOGIN,
    payload: { login: user.email }
  })

  history.push('/market')
}

export const logout = () => (dispatch) => {
  localStorage.removeItem('my-store:user')

  dispatch({
    type: LOGOUT
  })

  history.push('/login')
}

export const fetchProducts = () => async (dispatch) => {
  dispatch({
    type: FETCH_PRODUCTS_REQUEST
  })

  try {
    const products = await getAllProducts()

    dispatch({
      type: FETCH_PRODUCTS_SUCCESS,
      payload: products
    })
  } catch (error) {
    console.log(error)
  }
}

export const addToCart = (id) => {
  return {
    type: ADD_TO_CART,
    payload: id
  }
}

export const deleteFromCart = (id) => {
  return {
    type: DELETE_FROM_CART,
    payload: id
  }
}
