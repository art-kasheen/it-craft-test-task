export const getUser = (state) => state.user

export const getProduct = (state, id) => state.products.data[id]
export const getProducts = (state) => {
  return state.products.data
    ? Object.keys(state.products.data).map((id) => getProduct(state, id))
    : null
}

export const isProductInCart = (state, id) => state.cart.products.includes(id)

export const getProductsFromCart = (state) =>
  state.cart.products.map((id) => getProduct(state, id))

export const getCartAmount = (state) => state.cart.products.length
